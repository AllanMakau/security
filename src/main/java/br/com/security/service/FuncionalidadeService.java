/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.security.service;

import br.com.security.entity.Funcionalidade;
import br.com.security.repository.FuncionalidadeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan
 */

@Service
public class FuncionalidadeService {
    
    @Autowired
    private FuncionalidadeRepository repo;
    
    
    public List<Funcionalidade> findAll(){
        List<Funcionalidade> list = repo.findAll();
        return list;
    }
    
    public Funcionalidade findId(Long id){
        
        Funcionalidade funcionalidade = repo.findById(id).get();
        return funcionalidade;
    }
    
    public Funcionalidade insert(Funcionalidade obj){
    
        Funcionalidade funcionalidade = repo.save(obj);
        return funcionalidade;
    }
    
    public Funcionalidade update(Funcionalidade obj){
    
        Funcionalidade funcionalidade = repo.save(obj);
        return funcionalidade;
    }
    
    public void delete(Long id){
    
       repo.deleteById(id);
       
    }
    
    
    
}
