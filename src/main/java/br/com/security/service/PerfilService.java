/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.security.service;

import br.com.security.entity.Perfil;
import br.com.security.repository.PerfilRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Alan
 */
public class PerfilService {
   
    @Autowired
    private PerfilRepository repo;
    
    
    
    public List<Perfil> findAll(){
    
        List<Perfil> list = repo.findAll();
        return list;
    }
    
    public Perfil findId(Long id){
    
        Perfil perfil = repo.findById(id).get();
        return perfil;
    }
    
    public Perfil insert (Perfil obj){
        
        obj.setId(null);
        Perfil perfil = repo.save(obj);
        return perfil;
    }
    
    public Perfil update (Perfil obj){
    
        Perfil perfil = repo.save(obj);
        return perfil;
    }
    
    public void delete (Long id){
    
        repo.deleteById(id);
    }
}
