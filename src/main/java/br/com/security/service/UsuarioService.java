/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.security.service;

import br.com.security.entity.Usuario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.security.repository.UsuarioRepository;

/**
 *
 * @author Alan
 */
@Service
public class UsuarioService {
    
    @Autowired
    private UsuarioRepository repo;
    
     public List<Usuario> findAll(){
      
       return repo.findAll();
    }
     
     public Usuario findId(Long id){
         return repo.findById(id).get();
    }
     
     public Usuario insert(Usuario user){
     
         return repo.save(user);
     }
    
     public Usuario update(Usuario user){
     
        return repo.save(user);
     }
     
     public void delete(Long id){
         repo.deleteById(id);
     }

}
