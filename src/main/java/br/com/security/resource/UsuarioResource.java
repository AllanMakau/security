/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.security.resource;

import br.com.security.entity.Usuario;
import br.com.security.service.UsuarioService;
import java.net.URI;
import java.util.List;
import javax.xml.ws.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


/**
 *
 * @author Alan
 */

@RestController
@RequestMapping(value = "/usuarios")
public class UsuarioResource {
    
    
    @Autowired
    private UsuarioService service;
    
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Usuario>> findAll(){
        
        List<Usuario> objcts =service.findAll();
        return ResponseEntity.ok(objcts);
    }
    

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Usuario> findId(@PathVariable Long id){
    
        Usuario user = service.findId(id);
        return ResponseEntity.ok(user);
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> insert(@RequestBody Usuario user){
    
        service.insert(user);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Void> update( @RequestBody Usuario user){
    
        service.update(user);
        return ResponseEntity.noContent().build();
    }
    
    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(Long id){
    
        service.delete(id);
        return  ResponseEntity.noContent().build();
    }
}
