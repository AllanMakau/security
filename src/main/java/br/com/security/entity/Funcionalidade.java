/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.security.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;


/**
 *
 * @author Alan
 */

@Entity
public class Funcionalidade implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "funcionalidade_id")
    private Long id;
    private String chave;
    private String descricao;
    
    @ManyToMany()
   @JoinTable(name = "Perfil_Funcionalidade", joinColumns = @JoinColumn(name = "id_perfil"), 
           inverseJoinColumns =  @JoinColumn( name = "id_funcionalidade"))
    private List<Perfil> perfis;
    
    public Funcionalidade() {
    }

    public Funcionalidade(Long id, String chave, String descricao) {
        this.id = id;
        this.chave = chave;
        this.descricao = descricao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Perfil> getPerfis() {
        return perfis;
    }

    public void setPerfis(List<Perfil> perfis) {
        this.perfis = perfis;
    }

  


    
    
    
}
