/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.security.SecurityUtil;

import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 *
 * @author Alan
 */

@Configuration
@EnableSpringConfigured
public class SecurityConfig extends WebSecurityConfigurerAdapter{
 
    
    
    private static final String [] PUBLIC_MATCHERS ={
        "/h2-console/**"
    };
    
    private static final String [] PUBLIC_MATCHERS_GET ={
        "/usuarios/**"
    };
    
    @Override
    public void configure(HttpSecurity http) throws Exception{
        
        //desabilita proteção  csrf
        http.cors().and().csrf().disable();
        http.headers().frameOptions().disable();
        http.authorizeRequests()
                  .antMatchers(PUBLIC_MATCHERS).permitAll()
                  .antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).permitAll()
                  .anyRequest().authenticated();
        
        //Assegura que o backend não irá criar sessão de usuário.
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
    
    @Bean
    CorsConfigurationSource corsConfigurationSource(){
    
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }
    
    @Bean
    public BCryptPasswordEncoder bcryptPasswordEncoder(){
        
        return new BCryptPasswordEncoder();
    }
    
}
