package br.com.security;

import br.com.security.entity.Funcionalidade;
import br.com.security.entity.Perfil;
import br.com.security.entity.Usuario;
import br.com.security.repository.FuncionalidadeRepository;
import br.com.security.repository.PerfilRepository;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import br.com.security.repository.UsuarioRepository;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class SecurityApplication implements CommandLineRunner {

    @Autowired
    private UsuarioRepository repo;
    
    @Autowired
    private PerfilRepository per;
    
    @Autowired
    private FuncionalidadeRepository func;
    
    @Autowired
    private BCryptPasswordEncoder encode;
    
	public static void main(String[] args) {
		SpringApplication.run(SecurityApplication.class, args);
	}
        
        @Override
        public void run(String... args) throws Exception {
            
        Funcionalidade f1 = new Funcionalidade(null, "ROLES_INSERTUSER", "cadastrar usuarios");
        Funcionalidade f2 = new Funcionalidade(null, "ROLES_DELETEUSER", "deletar usuarios");
        Funcionalidade f3 = new Funcionalidade(null, "ROLES_LISTUSER", "listar usuarios");
        Funcionalidade f4 = new Funcionalidade(null, "ROLES_UPDATEUSER", "atualizar usuarios");
        Funcionalidade f5 = new Funcionalidade(null, "ROLES_INSERTPERFIL", "cadastrar perfil");
        Funcionalidade f6 = new Funcionalidade(null, "ROLES_DELETEPERFIL", "deletar perfil");
        Funcionalidade f7 = new Funcionalidade(null, "ROLES_UPDATEPERFIL", "atualizar Perfil");
        Funcionalidade f8 = new Funcionalidade(null, "ROLES_LISTPERFIL", "listar perfil");  
        Funcionalidade f9 = new Funcionalidade(null, "ROLES_INSERTFUNC", "cadastrar funcionalidade");
        Funcionalidade f10 = new Funcionalidade(null, "ROLES_DELETEFUNC", "cadastrar deletar");
        Funcionalidade f11 = new Funcionalidade(null, "ROLES_UPDATEFUNC", "cadastrar atualizar");
        Funcionalidade f12 = new Funcionalidade(null, "ROLES_LISTFUNC", "cadastrar listar");
        Funcionalidade mestre = new Funcionalidade(null, "ROLES_ADMIN", "Administrador");
        
        func.saveAll(Arrays.asList(f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,mestre));
            

        
        
        
        
        Perfil p1 = new Perfil(null, "ADMIN");
        Perfil p2 = new Perfil(null, "DIRETOR");
        Perfil p3 = new Perfil(null, "COORDENADOR");
        Perfil p4 = new Perfil(null, "SUPERVISOR");
        Perfil p5 = new Perfil(null, "TECNICO"); 
        
       
        
        
        per.saveAll(Arrays.asList(p1,p2,p3,p4,p5));
        
        
         Funcionalidade f = func.findById(13l).get();
        
        p1.getFuncionalidades().add(f);
        
        per.save(p1);
 
        
 
     

            
        
        Usuario user1 = new Usuario(null, "Alan Lima", "ajlima", encode.encode("123") , "Alan@gmail.com");
       // user1.getPerfis().add(p1);
        Usuario user2 = new Usuario(null, "Juliana Ramos", "jramos", encode.encode("321"), "juliana@gmail.com");
       // user2.getPerfis().add(p2);
        Usuario user3 = new Usuario(null, "Gilberto Gonzaga", "ggonzaga", encode.encode("123"), "gilberto@gmail.com");
       // user3.getPerfis().add(p3);
        Usuario user4 = new Usuario(null, "Valentina Graga", "vbraga", encode.encode("321"), "valentina@gmail.com");
       // user4.getPerfis().add(p4);
        repo.saveAll(Arrays.asList(user1,user2,user3,user4));
            
        
        
            
        }
    
}
